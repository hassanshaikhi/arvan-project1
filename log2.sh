 #!/bin/bash
#log=log.log
current_pwd=$(pwd)
#Colors
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m'
OLDIFS=$IFS
request_service=$(echo awk "{print"$\7"}")
request_host=$(echo awk "{print"$\8"}")
request_code=$(echo awk "{print"$\10"}")
count_200=0
count_300=0
count_400=0
count_500=0
rm -f tmp200.txt tmp300.txt tmp400.txt tmp500.txt
export -f code
  declare -ag code_array_200
  declare -ag code_array_300
  declare -ag code_array_400
  declare -ag code_array_500


request_ip(){
  awk '{print $5}' \
  | cut -d: -f1
} 
request_method(){
  awk '{print $17}' \
  |tr -s '\\"' ' '
}
request_date(){
  awk '{print $6}' \
  | tr -s '\\"' ' ' \
  | cut -d: -f1 \
  | cut -d[ -f2
}
request_user_epoch(){
  STARTSTAMP_USER="$( \
  date -d "$(echo "$start" \
  | sed -e 's,/,-,g' -e 's,:, ,')" +"%s")"
  ENDSTAMP_USER=$( \
  date -d "$(echo "$end" \
  | sed -e 's,/,-,g' -e 's,:, ,')" +"%s")
  awk '$0 >=from && $0 <=to' from="$STARTSTAMP_USER" to="$ENDSTAMP_USER" 
}
epoch_sed(){
  sed -e 's,/,-,g' -e 's,:, ,'
}

get_input(){

read -p "Start Date:(ex:2020-01-01) " start
read -p "End Date:(ex:2020-11-01) " end 

}


get_log_file_name(){
declare -a log_array
declare -ga files_array
count=0
date_log="${log_array[*]}" | cut -d. -f2 | awk '$0 >=from && $0 <=to' from="$START_USER" to="$END_USER" 
echo "Enter File Log Directory"
echo "Current Directory: $current_pwd"
echo "Press Enter To Use Current Directory"
read -p 'Log Path: ' log_path
if [[ -z $log_path ]]; then
log_path=$current_pwd
fi

read -p 'start: ' start
read -p 'end: ' end
START_USER=$(echo $start | tr -d - )
END_USER=$(echo $end | tr -d - )



get_files(){
    ls -1 $log_path |  grep -e '^haproxy.*.log.gz$' | cut -d. -f2 | awk '$0 >=from && $0 <=to' from="$START_USER" to="$END_USER" 
}

for i in $(get_files)
 do
   files_array+=("haproxy_latest.$i.log.gz")
done

#read -p 'start' START_USER=
#read -p 'end' END_USER=


IFS=$'\n'
#echo "${files_array[*]}" 

}


  

get_log_file_name
#echo "${files_array[*]}" 


code(){
    j=$1
    IFS=$'\n'
    for line in $(zcat $j \
    | grep HTTP/1.1) 
    do
   
    IFS=$OLDIFS
  #  echo "j= "$j
        response_code=$(echo "$line" \
        | $request_code )
  #  echo "i= $response_code"
    if [[ $response_code == [2][0-9][0-9] ]]; then
        code_array_200+=($response_code)
    
    elif [[ $req_response_code == [3][0-9][0-9] ]]; then
        code_array_300+=($response_code)
        
    elif [[ $response_code == [4][0-9][0-9] ]]; then
        code_array_400+=($response_code)
        
    elif [[ $response_code == [5][0-9][0-9] ]]; then
        code_array_500+=($response_code)
    fi

   # echo -ne '#'
    done


count_200="${#code_array_200[@]}"
count_300="${#code_array_300[@]}"
count_400="${#code_array_400[@]}"
count_500="${#code_array_500[@]}"

echo $count_200 >> tmp200.txt
echo $count_300 >> tmp300.txt
echo $count_400 >> tmp400.txt
echo $count_500 >> tmp500.txt








}









get_http_code(){

export -f code
  local log_date
  IFS=$'\n'

for j in "${files_array[@]}" 
do
IFS=$'\n'
 # echo "j= "$j
code $j &

P1=$!
done
wait 
echo -e '\n'
IFS=$'\n'
while read -r line;
do
 count_200=$(echo "$line+$count_200" | bc)
done < tmp200.txt

while read -r line;
do
 count_300=$(echo "$line+$count_300" | bc)
done < tmp300.txt

while read -r line;
do
 count_400=$(echo "$line+$count_400" | bc)
done < tmp400.txt

while read -r line;
do
 count_500=$(echo "$line+$count_500" | bc)
done < tmp500.txt
echo -e "${GREEN}$start 00:00:00 $end 00:00:00 2XX:$count_200 3XX:$count_300 4XX:$count_400 5XX:$count_500 ${NC} "
 

}








top_host(){
  
  declare -a host_array
  local count=0
  local log_date
  local arg1=$1
  local arg2=$2
  IFS=$'\n'

get_input

for line in $(zcat $log \
  | grep HTTP/1.1) 
do
  IFS=$OLDIFS
  log_date=$(echo "$line" \
	  |request_date )
    epoch_time=$(date -d "$(echo "$log_date" \
	  | epoch_sed)" +"%s")
	response_code=$(echo "$line" \
	  | $request_code )
    host=$(echo "$line" \
	  | $request_host )
	req=$(echo "$epoch_time $response_code $host" \
	  | request_user_epoch)
 	req_response_code=$(echo "$req" \
	  | awk '{print $2}' \
	  | sed '/^$/d')
    if [[ $req_response_code -ge $arg1 ]] && \
       [[ $req_response_code -lt $arg2 ]]; then
    req_host=$(echo "$req" \
	  | awk '{print $3}' \
	  | sed '/^$/d')
    host_array[$count]=$(echo "$req_host")
    count=$(echo $count+1 | bc)
  fi		 
echo -ne '#'
done
IFS=$'\n'
echo -e "\n"
echo "${host_array[*]}" \
  | sort \
  | uniq -c \
  | sed 's/^[ \t]*//' \
  | sort -r -t' ' -nk1 \
  | head
}



top_service(){
  declare -a service_array
  local count=0
  local log_date
  local arg1=$1
  local arg2=$2
  IFS=$'\n'
get_input
for line in $(zcat $log \
  | grep HTTP/1.1) 
do
  IFS=$OLDIFS
	log_date=$(echo "$line" \
	  |request_date )
    epoch_time=$(date -d "$(echo "$log_date" \
	  | epoch_sed)" +"%s")
	response_code=$(echo "$line" \
	  | $request_code )
    service=$(echo "$line" \
	  | $request_service )
	req=$(echo "$epoch_time $response_code $service" \
	  | request_user_epoch)
 	req_response_code=$(echo "$req" \
	  | awk '{print $2}' \
	  | sed '/^$/d')
    if [[ $req_response_code -ge "$arg1" ]] && \
       [[ $req_response_code -lt "$arg2" ]]; then
    req_service=$(echo "$req" \
	  | awk '{print $3}' \
	  | sed '/^$/d')
    service_array[$count]=$(echo "$req_service")
    count=$(echo $count+1 | bc)
  fi		 
echo -ne '#'
done
IFS=$'\n'
echo -e "\n"
echo "${service_array[*]}" \
  | sort \
  | uniq -c \
  | sed 's/^[ \t]*//' \
  | sort -r -t' ' -nk1 \
  | head
}




top_ip(){
  declare -a ip_array
  local count=0
  local log_date
  local arg1=$1
  local arg2=$2
  IFS=$'\n'
get_input 
for line in $(zcat $log \
  | grep HTTP/1.1) 
do
  IFS=$OLDIFS
	log_date=$(echo "$line" \
	  |request_date )
    epoch_time=$(date -d "$(echo "$log_date" \
	  | epoch_sed)" +"%s")
	response_code=$(echo "$line" \
	  | $request_code )
    ip=$(echo "$line" \
	  | request_ip )
	req=$(echo "$epoch_time $response_code $ip" \
	  | request_user_epoch)
 	req_response_code=$(echo "$req" \
	  | awk '{print $2}' \
	  | sed '/^$/d')
    if [[ $req_response_code -ge $arg1 ]] && \
       [[ $req_response_code -lt $arg2 ]]; then
    req_ip=$(echo "$req" \
	  | awk '{print $3}' \
	  | sed '/^$/d')
    ip_array[$count]=$(echo "$req_ip")
    count=$(echo $count+1 | bc)
  fi		 
echo -ne '#'
done
IFS=$'\n'
echo -e "\n"
echo "${ip_array[*]}" \
  | sort \
  | uniq -c \
  | sed 's/^[ \t]*//' \
  | sort -r -t' ' -nk1 \
  | head
}





get_http_method()
{
  Method_GET_200=0
  Method_POST_200=0
  Method_HEAD_200=0
  Method_CONNECT_200=0
  Method_OPTIONS_200=0
  Method_TRACE_200=0
  Method_PUT_200=0
  Method_PATCH_200=0

  Method_GET_300=0
  Method_POST_300=0
  Method_HEAD_300=0
  Method_CONNECT_300=0
  Method_OPTIONS_300=0
  Method_TRACE_300=0
  Method_PUT_300=0
  Method_PATCH_300=0

  Method_GET_400=0
  Method_POST_400=0
  Method_HEAD_400=0
  Method_CONNECT_400=0
  Method_OPTIONS_400=0
  Method_TRACE_400=0
  Method_PUT_400=0
  Method_PATCH_400=0

  Method_GET_500=0
  Method_POST_500=0
  Method_HEAD_500=0
  Method_CONNECT_500=0
  Method_OPTIONS_500=0
  Method_TRACE_500=0
  Method_PUT_500=0
  Method_PATCH_500=0

  local log_date
  IFS=$'\n'
get_input 
for line in $(zcat $log \
  | grep HTTP/1.1) 
do
  IFS=$OLDIFS
	log_date=$(echo "$line" \
	  |request_date )
    epoch_time=$(date -d "$(echo "$log_date" \
	  | epoch_sed)" +"%s")
	response_code=$(echo "$line" \
	  | $request_code )
    method=$(echo "$line" \
	  | request_method )
	req=$(echo "$epoch_time $response_code $method" \
	  | request_user_epoch)
 	req_response_code=$(echo "$req" \
	  | awk '{print $2}' \
	  | sed '/^$/d')
    req_method=$(echo "$req" \
	  | awk '{print $3}' \
	  | sed '/^$/d')  
    if [[ $req_response_code -ge "200" ]] && \
       [[ $req_response_code -lt "300" ]]; then
     
      case $req_method in
            GET) Method_GET_200=$(echo "$Method_GET_200+1" | bc );;
            POST) Method_POST_200=$( echo "$Method_POST_200+1" | bc );;
            HEAD) Method_HEAD_200=$( echo "$Method_HEAD_200+1" | bc );;
            CONNECT) Method_CONNECT_200=$(echo "$Method_CONNECT_200+1" | bc );;
            OPTIONS) Method_OPTIONS_200=$(echo "$Method_OPTIONS_200+1" | bc );;
            TRACE) Method_TRACE_200=$(echo "$Method_TRACE_200+1" | bc);;
            PUT) Method_PUT_200=$(echo "$Method_PUT_200+1" | bc );;
            PATCH) Method_PATCH_200=$(echo "$Method_PATCH_200+1" | bc );;
	  esac 
 		  
    elif [[ $req_response_code -ge "300" ]] && \
         [[ $req_response_code -lt "400" ]]; then

      case $req_method in
            GET) Method_GET_300=$(echo "$Method_GET_300+1" | bc );;
            POST) Method_POST_300=$( echo "$Method_POST_300+1" | bc );;
            HEAD) Method_HEAD_300=$( echo "$Method_HEAD_300+1" | bc );;
            CONNECT) Method_CONNECT_300=$(echo "$Method_CONNECT_300+1" | bc );;
            OPTIONS) Method_OPTIONS_300=$(echo "$Method_OPTIONS_300+1" | bc );;
            TRACE) Method_TRACE_300=$(echo "$Method_TRACE_300+1" | bc);;
            PUT) Method_PUT_300=$(echo "$Method_PUT_300+1" | bc );;
            PATCH) Method_PATCH_300=$(echo "$Method_PATCH_300+1" | bc );;
	  esac  

    elif [[ $req_response_code -ge "400" ]] && \
         [[ $req_response_code -lt "500" ]]; then

      case $req_method in
            GET) Method_GET_400=$(echo "$Method_GET_400+1" | bc );;
            POST) Method_POST_400=$( echo "$Method_POST_400+1" | bc );;
            HEAD) Method_HEAD_400=$( echo "$Method_HEAD_400+1" | bc );;
            CONNECT) Method_CONNECT_400=$(echo "$Method_CONNECT_400+1" | bc );;
            OPTIONS) Method_OPTIONS_400=$(echo "$Method_OPTIONS_400+1" | bc );;
            TRACE) Method_TRACE_400=$(echo "$Method_TRACE_400+1" | bc);;
            PUT) Method_PUT_400=$(echo "$Method_PUT_400+1" | bc );;
            PATCH) Method_PATCH_400=$(echo "$Method_PATCH_400+1" | bc );;
	  esac 
 
    elif [[ $req_response_code -ge "500" ]] && \
         [[ $req_response_code -lt "600" ]]; then

      case $req_method in
            GET) Method_GET_500=$(echo "$Method_GET_500+1" | bc );;
            POST) Method_POST_500=$( echo "$Method_POST_500+1" | bc );;
            HEAD) Method_HEAD_500=$( echo "$Method_HEAD_500+1" | bc );;
            CONNECT) Method_CONNECT_500=$(echo "$Method_CONNECT_500+1" | bc );;
            OPTIONS) Method_OPTIONS_500=$(echo "$Method_OPTIONS_500+1" | bc );;
            TRACE) Method_TRACE_500=$(echo "$Method_TRACE_500+1" | bc);;
            PUT) Method_PUT_500=$(echo "$Method_PUT_500+1" | bc );;
            PATCH) Method_PATCH_500=$(echo "$Method_PATCH_500+1" | bc );;
	  esac 
  	fi
echo -ne '#'
done
Method_GET_SUM=$(echo "$Method_GET_200+$Method_GET_300+$Method_GET_400+$Method_GET_500" |bc)
Method_POST_SUM=$(echo "$Method_POST_200+$Method_POST_300+$Method_POST_400+$Method_POST_500" | bc)
Method_PUT_SUM=$(echo "$Method_PUT_200+$Method_PUT_300+$Method_PUT_400+$Method_PUT_500" | bc)
Method_OPTIONS_SUM=$(echo "$Method_OPTIONS_200+$Method_OPTIONS_300+$Method_OPTIONS_400+$Method_OPTIONS_500" | bc)
Method_HEAD_SUM=$(echo "$Method_HEAD_200+$Method_HEAD_300+$Method_HEAD_400+$Method_HEAD_500" | bc)
Method_TRACE_SUM=$(echo "$Method_TRACE_200+$Method_TRACE_300+$Method_TRACE_400+$Method_TRACE_500" |bc)
Method_CONNECT_SUM=$(echo "$Method_CONNECT_200+$Method_CONNECT_300+$Method_CONNECT_400+$Method_CONNECT_500" |bc)
Method_PATCH_SUM=$(echo "$Method_PATCH_200+$Method_PATCH_300+$Method_PATCH_400+$Method_PATCH_500" |bc)
echo -e '\n'
echo -e "${RED}2XX GET:$Method_GET_200 POST:$Method_POST_200 PUT:$Method_PUT_200 HEAD:$Method_HEAD_200 OPTIONS:$Method_OPTIONS_200 TRACE:$Method_TRACE_200  CONNECT:$Method_CONNECT_200 PATCH:$Method_PATCH_200  "
echo -e "${YELLOW}3XX GET:$Method_GET_300 POST:$Method_POST_300 PUT:$Method_PUT_300 HEAD:$Method_HEAD_300 OPTIONS:$Method_OPTIONS_300 TRACE:$Method_TRACE_300  CONNECT:$Method_CONNECT_300 PATCH:$Method_PATCH_300 "
echo -e "${BLUE}4XX GET:$Method_GET_400 POST:$Method_POST_400 PUT:$Method_PUT_400 HEAD:$Method_HEAD_400 OPTIONS:$Method_OPTIONS_400 TRACE:$Method_TRACE_400  CONNECT:$Method_CONNECT_400 PATCH:$Method_PATCH_400 "
echo -e "${PURPLE}5XX GET:$Method_GET_500 POST:$Method_POST_500 PUT:$Method_PUT_500 HEAD:$Method_HEAD_500 OPTIONS:$Method_OPTIONS_500 TRACE:$Method_TRACE_500  CONNECT:$Method_CONNECT_500 PATCH:$Method_PATCH_500 "
echo -e "${CYAN}ALL GET:$Method_GET_SUM POST:$Method_POST_SUM PUT:$Method_PUT_SUM HEAD:$Method_HEAD_SUM OPTIONS:$Method_OPTIONS_SUM TRACE:$Method_TRACE_SUM  CONNECT:$Method_CONNECT_SUM  PATCH:$Method_PATCH_SUM ${NC} "
}

submenu_ip () {
  local PS3='Please Enter ip Option: '
  local options=("Top ip 2XX" "Top ip 3XX" "Top ip 3XX" "Top ip 4XX" "Main Menu")
  local opt
  select opt in "${options[@]}"
  do
    case $opt in
        "Top ip 2XX")
            top_ip 200 300
            ;;
        "Top ip 3XX")
            top_ip 300 400
            ;;
        "Top ip 4XX")
            top_ip 400 500
            ;;
	    "Top ip 5XX")
            top_ip 500 600
            ;;
        "Main Menu")
		    echo "Press Enter to Show options"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac

  done
}

submenu_service () {
  local PS3='Please Enter Service Options: '
  local options=("Top Service 2XX" "Top Service 3XX" "Top Service 4XX" "Top Service 5XX" "Main Menu")
  local opt
  select opt in "${options[@]}"
  do
    case $opt in
        "Top Service 2XX")
            top_service 200 300
            ;;
        "Top Service 3XX")
            top_service 300 400
            ;;
        "Top Service 4XX")
            top_service 400 500
            ;;
	    "Top Service 5XX")
            top_service 500 600
            ;;
        "Main Menu")
		    echo "Press Enter to Show options"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac

  done
}
submenu_host () {
  local PS3='Please Enter Host Option: '
  local options=("Top Host 2XX" "Top Host 3XX" "Top Host 4XX" "Top Host 5XX" "Main Menu")
  local opt
  select opt in "${options[@]}"
  do
    case $opt in
        "Top Host 2XX")
            top_host 200 300
            ;;
        "Top Host 3XX")
            top_host 300 400
            ;;
        "Top Host 4XX")
            top_host 400 500
            ;;
	    "Top Host 5XX")
            top_host 500 600
            ;;
        "Main Menu")
		    echo "Press Enter to Show options"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac

  done
}

COLUMNS=12

# main menu
PS3='Please Enter Main Option : '
options=("Get HTTP Response Status Codes" "Get HTTP Request Methods" "Top IPs" "Top Services" "Top Hosts" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Get HTTP Response Status Codes")
          time get_http_code 
            ;;
        "Get HTTP Request Methods")
            get_http_method
            ;;
        "Top IPs")
            submenu_ip
            ;;
       "Top Services")
            submenu_service
            ;;
	   "Top Hosts")
            submenu_host
            ;;
        "Quit")
            exit
            ;;
        *) #echo "invalid option $REPLY";;
            PS3="" 
            echo asdf | select foo in "${options[@]}"; do break; done 
            PS3="invalid option " 
            ;;
    esac
done
