 #!/bin/bash
#log=log.log
current_pwd=$(pwd)
#Colors
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m'
OLDIFS=$IFS

rm -f tmp200.txt tmp300.txt tmp400.txt tmp500.txt

  declare -ag code_array_200
  declare -ag code_array_400
  declare -ag code_array_500
count_200=0
count_400=0
count_500=0


get_input(){

read -p "Start Date:(ex:2020-01-01) " start
read -p "End Date:(ex:2020-11-01) " end 

}


get_log_file_name(){
declare -a log_array
declare -ga files_array
count=0
date_log="${log_array[*]}" | cut -d. -f2 | awk '$0 >=from && $0 <=to' from="$START_USER" to="$END_USER" 
#echo "Enter File Log Directory"
#echo "Current Directory: $current_pwd"
#echo "Press Enter To Use Current Directory"
#read -p 'Log Path: ' log_path
#if [[ -z $log_path ]]; then
log_path=$current_pwd
#fi


#read -p 'start: ' start
#read -p 'end: ' end
start="2020-09-01"
end="2020-09-16"
START_USER=$(echo $start | tr -d - )
END_USER=$(echo $end | tr -d - )



get_files(){
    ls -1 $log_path |  grep -e '^haproxy.*.log.gz$' | cut -d. -f2 | awk '$0 >=from && $0 <=to' from="$START_USER" to="$END_USER" 
}

for i in $(get_files)
 do
   files_array+=("haproxy_latest.$i.log.gz")
done

#read -p 'start' START_USER=
#read -p 'end' END_USER=


IFS=$'\n'
#echo "${files_array[*]}" 

}


  

get_log_file_name
#echo "${files_array[*]}" 


code(){
    j=$1
    

codes=$(zcat $j | grep "HTTP/1.1" | awk '{print $10}')
code_array=$(echo "${codes[@]}" | sort | uniq -c| sort -n -r -t ' ' -k1) 
 
 #   IFS=$OLDIFS


   IFS=$'\n'
for response_code in $(echo "${code_array[*]}")
do
    req_code=$(echo $response_code | awk '{print $2}')
    req_count=$(echo $response_code | awk '{print $1}')

  if [[ $req_code == [2][0-9][0-9] ]]; then
        code_array_200+=($req_count)
        
    elif [[ $req_code == [4][0-9][0-9] ]]; then
        code_array_400+=($req_count)
        
    elif [[ $req_code == [5][0-9][0-9] ]]; then
        code_array_500+=($req_count)
    fi

  done

date_log=$(echo $j | cut -d. -f2)
year=$(echo $date_log | grep -Eo ^[0-9][0-9][0-9][0-9])
monthday=$(echo $date_log | grep -Eo [0-1][0-9][0-3][0-9]$)
month=$(echo $monthday | grep -Eo ^[0-1][0-9])
day=$(echo $monthday | grep -Eo [0-3][0-9]$)
datelog="$year-$month-$day"

count_200=$(IFS="+";bc<<<"${code_array_200[*]}")
count_400=$(IFS="+";bc<<<"${code_array_400[*]}")
count_500=$(IFS="+";bc<<<"${code_array_500[*]}")

echo -e "${GREEN}$datelog: 2XX:$count_200 4XX:$count_400 5XX:$count_500 ${NC} "

echo $count_200 >> tmp200.txt
echo $count_400 >> tmp400.txt
echo $count_500 >> tmp500.txt



}






get_http_code(){


  local log_date
  IFS=$'\n'

for j in "${files_array[@]}" 
do
IFS=$'\n'
 # echo "j= "$j
code $j &


done
wait 
echo -e '\n'


IFS=$'\n'
while read -r line;
do
 count_200=$(echo "$line+$count_200" | bc)
done < tmp200.txt


while read -r line;
do
 count_400=$(echo "$line+$count_400" | bc)
done < tmp400.txt

while read -r line;
do
 count_500=$(echo "$line+$count_500" | bc)
done < tmp500.txt
echo -e "َALL: ${GREEN}$start 00:00:00 $end 00:00:00 2XX:$count_200 3XX:$count_300 4XX:$count_400 5XX:$count_500 ${NC} "










}




get_http_code
