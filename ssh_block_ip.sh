#!/bin/bash
declare ip_array
OLDIFS=$IFS
log=/var/log/secure
MAX_RETRY=3 # max password wrong
client_ip=$(echo $SSH_CLIENT | awk '{ print $1}')
#cat $log | grep "Failed password" |grep -Eo from.*| awk '{print $2}'
ip_array=$(cat $log |  grep "Failed password" |grep -Eo from.* | awk '{print $2}' )  # grep wrong password input match
ip2_array=$(echo "${ip_array[@]}" | sort | uniq -c| sort -n -r -t ' ' -k1| sed 's/^[ \t]*//') # put all ip to array

#echo "${ip2_array[*]}"


IFS=$'\n'

for i in  $(echo "${ip2_array[*]}")
do
count=$(echo $i | awk '{print $1}')
ip=$(echo $i | awk '{print $2}')
if [[ $count -ge $MAX_RETRY ]] && [[ $ip != $client_ip ]] ; then
iptables -A INPUT -s $ip -p tcp --dport 22 -j DROP
fi

done

